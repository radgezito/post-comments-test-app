<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCommentRequest;
use App\Http\Resources\CommentResource;
use App\Http\Resources\PostResource;
use App\Models\Comment;
use App\Models\Post;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Http;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
        //return CommentResource::collection(Comment::all());
    }


    /**
     * Display the specified resource.
     *
     * @return Application|ResponseFactory|AnonymousResourceCollection|Response
     */
    public function show($post_id)
    {
        $data = Http::get('https://jsonplaceholder.typicode.com/posts/'.$post_id.'/comments')->json();

        if (empty($data)) {
            return response('Post ID not found', 404)
                ->header('Content-Type', 'text/plain');
        }

        return CommentResource::collection($data);
    }
}
