<?php

namespace App\Http\Controllers;

use App\Http\Resources\PostResource;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Http;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|ResponseFactory|Response
     */
    public function index()
    {
        $data = Http::get('https://jsonplaceholder.typicode.com/posts')->json();

        if (empty($data)) {
            return response('Something has wrong retreiving information from jsonplaceholder', 404)
                ->header('Content-Type', 'text/plain');
        }

        return PostResource::collection($data);
    }


    /**
     * Display the specified resource.
     *
     * @return Response
     */
    public function show($post_id)
    {
        $data = Http::get('https://jsonplaceholder.typicode.com/posts/'.$post_id)->json();

        if (empty($data)) {
            return response('Post ID not found', 404)
                ->header('Content-Type', 'text/plain');
        }

        return new PostResource($data);
    }

}
